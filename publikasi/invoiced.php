<?php
function rp($angka){
	    $konversi = 'Rp '.number_format($angka, 0, ',', '.');
	    return $konversi;
	}
	
function hr($x){
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return hr($x - 10) . " belas";
  elseif ($x < 100)
    return hr($x / 10) . " puluh" . hr($x % 10);
  elseif ($x < 200)
    return " seratus" . hr($x - 100);
  elseif ($x < 1000)
    return hr($x / 100) . " ratus" . hr($x % 100);
  elseif ($x < 2000)
    return " seribu" . hr($x - 1000);
  elseif ($x < 1000000)
    return hr($x / 1000) . " ribu" . hr($x % 1000);
  elseif ($x < 1000000000)
    return hr($x / 1000000) . " juta" . hr($x % 1000000);
	
	}
//include('../koneksi/fungsi.php');
include('../koneksi/koneksi.php');
include('../koneksi/query.php');
$qC = new qC;
 // Define relative path from this script to mPDF
 $nama_dokumen='rekap_mobil'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../asset/MPDF57/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8', 'A4'); 
$mpdf->SetDisplayMode('fullpage','two');// Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags
ob_start();
?>
<!--sekarang Tinggal Codeing seperti biasanya. HTML, CSS, PHP tidak masalah.-->
<!--CONTOH Code START-->
<link rel="stylesheet" href="../asset/MPDF57/mpdf.css" type="text/css">
<?php $no = 1;
if(isset($_GET['id_p']) && ($_GET['tgl'])){
	$id_pd = $_GET['id_p'];
	$tgll = $_GET['tgl'];
	$data = $qC->dmrlinvoicea_peminjaman($id_pd,$tgll);
	$data2 = $qC->dmrlinvoiceb_peminjaman($id_pd,$tgll);
	$tgl = date('m', strtotime($data['tanggal_transaksi']));
	$thn = date('Y', strtotime($data['tanggal_transaksi']));
	if($tgl==1){ $tgl_b ="I";}else if($tgl==2){$tgl_b ="II";}else if($tgl==3){$tgl_b ="III";}else if($tgl==4){$tgl_b ="IV";}else if($tgl==5){$tgl_b ="V";}else if($tgl==6){$tgl_b ="VI";}else if($tgl==7){$tgl_b ="VII";}else if($tgl==8){$tgl_b ="VIII";}else if($tgl==9){$tgl_b ="IX";}else if($tgl==10){$tgl_b ="X";}else if($tgl==11){$tgl_b ="XI";}else if($tgl==12){$tgl_b ="XII";}
	$query = $pdo->prepare("SELECT SUM(total_biaya) AS tobaya FROM transaksi_dmrl WHERE id_pelanggan=? 
	AND tanggal_transaksi=?");$query->bindValue(1, $id_pd);$query->bindValue(2, $tgll);$query->execute();
		$tby = $query->fetchAll(); foreach($tby as $t){ $tobaya = $t['tobaya'];}
echo'
<table width="100%" class="miring">
<tr><td colspan="10"><div class="jdl_inv">PRADANA transport</div>
<div class="jdl_inv4">Rent Car & Microbus</div>
<div class="jdl_inv2">Jl. Raya Medokan Sawah 63</div>
<div class="jdl_inv2">Rungkut - Surabaya</div>
<div class="jdl_inv2">Hotline : 0878 532 7777 3, 0812 350 9999 1</div>
<div class="jdl_inv2">031 - 878 2323, 031 - 719 325 27</div>
</td><td><div class="tgl">No : '.$data['id_pd']. '/'.$tgl_b.'/PT/'.$thn.'</div>
<br><br><br><br><br><br>
</td></tr>
<tr><td colspan="15" class="ktns">K U I T A N S I</td></tr>
<tr><td>Sudah terima dari</td><td> : </td><td>'.$data['nama'].'</td></tr>
<tr><td>Jumlah Uang</td><td> : </td><td>'.ucwords(hr($tobaya)).' Rupiah</td></tr>
<tr><td></td></tr>
</table>
Buat pembayaran sewa kendaraan dengan rincian sebagai berikut :<br><br>
<table width="100%" border="1" class="tabel">
<tr class="trs"><td>No</td><td>Mobil </td><td>Tujuan</td><td>Tanggal Pinjam </td><td>Tanggal Kembali </td><td>Biaya</td></tr>
'; ?>
<?php
foreach($data2 as $d){
	echo'
<tr><td>'.$no++.'</td><td>'.$d['nama_mobil'].'</td><td>'.$d['tujuan'].'</td><td>'.date('j F, Y | g:i a',
 strtotime($d['tanggal_pinjam'])).'</td><td>'.date('j F, Y | g:i a',
 strtotime($d['tanggal_kembali'])).'</td><td>'.rp($d['total_biaya']).'</td></tr>
';} 
echo '<tr><td colspan="5"><b>Total Bayar</td><td><b>'.rp($tobaya).'</td></tr>';
?>
 </table>
<br>
<div class="jdl_inv3">Surabaya : <?php echo $tgl = date('d F Y', strtotime($data['tanggal_transaksi']));?></div>
<br />
<br />
<div class="jdl_inv3"><b>Januar Hamadhani P.</b></div>
<hr />
<!--CONTOH Code END-->
<?php }
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
