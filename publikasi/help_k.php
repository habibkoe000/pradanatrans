<?php

 // Define relative path from this script to mPDF
 $nama_dokumen='help'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../asset/MPDF57/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8', 'A4'); 
$mpdf->SetDisplayMode('fullpage','two');// Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags
ob_start();
?>
<!--sekarang Tinggal Codeing seperti biasanya. HTML, CSS, PHP tidak masalah.-->
<!--CONTOH Code START-->
<link rel="stylesheet" href="../asset/MPDF57/mpdf.css" type="text/css">
<div class="jdl_lap">Buku Panduan Kasir</div>
<br>
1. Menu Branda : <br>
<div class="help_p">
- Menu branda menampilkan 7 laporan peminjaman terbaru, terdiri dari peminjaman tunai dan peminjaman hutang.<br>
- Untuk mencetak Infoice atau Kuitansi anda bisa mengklik Icon <img src="../gambar/print.png">.<br>
- Untuk mencetak 7 laporan terbaru , anda bisa mengklik tombol merah "Cetak".
</div>
<hr />
2. Menu Data Pelanggan : <br>
<div class="help_p">
- Menu Data Pelanggan menampilkan data dari pelanggan atau penyewa mobil.<br>
- Sebelum melakukan transaksi peminjaman, Kasir terlebih dahulu masuk ke menu ini, bila data pelanggan belum ada, Kasir harus menginputkan data penyewa terlebih dahulu.<br>
- Untuk melakukan penginputan data penyewa, Kasir bisa mengklik tombol berwarna biru "Pelanggan Baru".<br>
- Bila data pelanggan sudah ada Klik tombok merah "Pesan Mobil".
</div>
<hr>
3. Menu Data Mobil : <br>
<div class="help_p">
- Menu Data Mobil menampilakan semua data mobil yang "ready".<br>
- Untuk melihat detail mobil, cukup dengan mengklik gambar mobil.
</div>
<hr>
4. Menu Transaksi Peminjaman : <br>
<div class="help_p">
- Menu Transaksi Peminjaman ini berisi form untuk melakukan transaksi.<br>
- Setelah masuk ke menu ini, Kasir bisa memilih penyewa yang akan melakukan penyewaan mobil, dengan syarat data penyewa sudah terinput terlebih dahulu.<br>
- Setelah itu memilih mobil yang di inginkan, setelah memilih mobil, Tarif dasar muncul secara otomatis.<br>
- Di samping kanan Kasir bisa menentukan tanggal mobil di pinjam, dan tanggal mobil di kembalikan, setelah di set tanggalnya , maka secara otomatis total jam akan muncul secara otomatis.<br>
- pada bagian "Biaya Tambahan", Kasir bisa mencentang bila :
	<div class="help_p">
	- Bahan bakar mobil sudah terisi oleh Pradana.<br>
    - Penyewa membutuhkan Supir.
	</div>
- untuk mengatur biaya BBM dan supir, Kasir bisa mengklik tombol <img src="../gambar/edit.png">.  <br> 
- pada bagian "Biaya", Kasir bisa memberikan Diskon secara manual.<br>
- untuk melihat Biaya Total, Kasir cukup mengklik Textfild di samping tulisan "Biaya Total".<br>
- pada bagian "Pembayaran", bila penyewa melakukan transaksi tunai, maka jumlah uang yang dimasukkan harus sama dengan Biaya Total, tapi jika melakukan Transaksi Hutang, Kasir bisa mamasukkan jumlah uang muka terlebih dahulu setelah itu klik tombol hitung.<br>
- Bila kolom "belum di bayar" terisi maka transaksi berupa transaksi hutang, namun bila kolom "belum di bayar" terisi 0, maka transaksi tunai.<br>
- pada bagian "Jenis Pembayaran", Kasir wajib mencentang "cash" kalau transaksi tunai, dan "kredit" kalau transaksi hutang.
</div>
<hr>
5. Menu Mobil Rental Lain : <br>
<div class="help_p">
- Menu Mobil Rental Lain menampilakan semua data mobil rental lain yang "ready".<br>
- Untuk melihat detail mobil, cukup dengan mengklik gambar mobil.<br>
</div>
<hr />
<br /><br /><br />
6. Menu Transaksi Rental Lain : <br>
<div class="help_p">
- Menu Transaksi Peminjaman ini berisi form untuk melakukan transaksi.<br>
- Setelah masuk ke menu ini, Kasir bisa memilih penyewa yang akan melakukan penyewaan mobil, dengan syarat data penyewa sudah terinput terlebih dahulu.<br>
- Setelah itu memilih Nama Rental yang di inginkan.<br>
- Setelah itu memilih mobil yang di inginkan, setelah memilih mobil, Tarif dasar muncul secara otomatis.<br>
- Di samping kanan Kasir bisa menentukan tanggal mobil di pinjam, dan tanggal mobil di kembalikan, setelah di set tanggalnya , maka secara otomatis total jam akan muncul secara otomatis.<br>
- pada bagian "Biaya Tambahan", Kasir bisa mencentang bila :
	<div class="help_p">
	- Bahan bakar mobil sudah terisi oleh Pradana.<br>
    - Penyewa membutuhkan Supir.
	</div>
- untuk mengatur biaya BBM dan supir, Kasir bisa mengklik tombol <img src="../gambar/edit.png">.  <br> 
- pada bagian "Biaya", Kasir bisa memberikan Diskon secara manual.<br>
- untuk melihat Biaya Total, Kasir cukup mengklik Textfild di samping tulisan "Biaya Total".<br>
- pada bagian "Pembayaran", bila penyewa melakukan transaksi tunai, maka jumlah uang yang dimasukkan harus sama dengan Biaya Total, tapi jika melakukan Transaksi Hutang, Kasir bisa mamasukkan jumlah uang muka terlebih dahulu setelah itu klik tombol hitung.<br>
- Bila kolom "belum di bayar" terisi maka transaksi berupa transaksi hutang, namun bila kolom "belum di bayar" terisi 0, maka transaksi tunai.<br>
- pada bagian "Jenis Pembayaran", Kasir wajib mencentang "cash" kalau transaksi tunai, dan "kredit" kalau transaksi hutang.
</div>
<hr />
7. Menu Pengambalian Mobil : <br>
<div class="help_p">
- pada menu ini terdapat tombol berwarna biru "Mobil Sendiri", dan "Mobil Rental Lain".<br />
- "Mobil Sendiri", menampilkan data pengembalian mobil pradana yang di pinjam.<br />
- "Mobil Rental Lain", menampilkan data pengembalian mobil rental lain yang di pinjam.<br />
- selain itu terdapat tombol merah di sebelah kanan "Lunas", "Hutang".<br />
- tombol "lunas" menampilkan data mobil yang di sewa secara cash.<br />
- tombol "hutang" menampilkan data mobil yang di sewa secara hutang.<br />
- Untuk melakukan pengembalian mobil, cukup mengklik gambar mobil dan muncul form.<br />
- Setelah masuk dalam form, Klik tombol "Cek Tanggal" untuk validasi tanggal kembali.<br />
- Setelah itu klik tombol "Mobil Kembali".
</div>
<hr />
8. Menu Lihat Peminjaman : <br>
<div class="help_p">
- dalam menu ini terdapat submenu "Tunai", "Hutang", "Rent Lain | Tunai", "Rent Lain | Hutang".<br />
- "Tunai", Kasir bisa melihat data transaksi Penyewaan "mobil pradana" yang bersifat tunai / cash.<br />
- "Hutang, Kasir bisa melihat data transaksi Penyewaan "mobil pradana" yang bersifat Hutang / kredit.<br />
- "Rent Lain | Tunai", Kasir bisa melihat data transaksi Penyewaan "mobil rental lain" yang bersifat tunai / cash.<br />
- "Rent Lain | Hutang, Kasir bisa melihat data transaksi Penyewaan "mobil rental lain" yang bersifat Hutang / kredit.<br />
- form cari berdasarkan bulan, untuk mencari data transaksi berdasarkan bulan.<br />
- form cari berdasarkan tanggal, untuk mencari dara transaksi berdasarkan tanggal.<br />
- untuk mencetak invoice / kuitansi bisa dengan mengklik icon <img src="../gambar/print.png" />.<br />
- untuk melakukan edit / merubah data transaksi bisa dengan mengklik icon <img src="../gambar/edit.png" />.<br />
- untuk menghapus data transaksi dengan mengklik icon <img src="../gambar/bs.png" />.<br />
- untuk mencetak semua data / data perbulan / data per tanggal bisa dengan mengklik tombol merah "Cetak Detail" di sudut kanan bawah. 
</div>
<hr />
9. Menu Akun Pengguna : <br>
<div class="help_p">
- dalam menu ini Kasir bisa merubah Akunnya sendiri.
</div>
<!--CONTOH Code END-->
<?php 
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
