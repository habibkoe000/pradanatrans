<?php
function rp($angka){
	    $konversi = 'Rp '.number_format($angka, 0, ',', '.');
	    return $konversi;
	}
	
function hr($x){
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return hr($x - 10) . " belas";
  elseif ($x < 100)
    return hr($x / 10) . " puluh" . hr($x % 10);
  elseif ($x < 200)
    return " seratus" . hr($x - 100);
  elseif ($x < 1000)
    return hr($x / 100) . " ratus" . hr($x % 100);
  elseif ($x < 2000)
    return " seribu" . hr($x - 1000);
  elseif ($x < 1000000)
    return hr($x / 1000) . " ribu" . hr($x % 1000);
  elseif ($x < 1000000000)
    return hr($x / 1000000) . " juta" . hr($x % 1000000);
	
	}
//include('../koneksi/fungsi.php');
include('../koneksi/koneksi.php');
include('../koneksi/query.php');
$qC = new qC;

 // Define relative path from this script to mPDF
 $nama_dokumen='rekap_mobil'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../asset/MPDF57/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8','A4','','',42,15,67,67,20,15); 
$mpdf->SetDisplayMode('fullpage','two');// Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags
ob_start();
?>
<!--sekarang Tinggal Codeing seperti biasanya. HTML, CSS, PHP tidak masalah.-->
<!--CONTOH Code START-->
<link rel="stylesheet" href="../asset/MPDF57/mpdf.css" type="text/css">
<div class="jdl_lap">Transaksi Pemakaian Mobil Hutang</div>
<div class="sub_jdl">Jalan Medokan Sawah 63 Telp/Fax : 031 8782313</div>
<?php
if(isset($_GET['id'])or($_GET['idb'])){
	$tggl = $_GET['id'];
	$bln = $_GET['idb'];
	if(empty($bln)){
	$bp = $qC->lihat_hutang($tggl);
	echo 'Tanggal Transaksi : '.date('d F, Y', strtotime($tggl)).'</b><br><br>'; 
	$query = $pdo->prepare("SELECT SUM(total_biaya) AS tobaya, SUM(biaya_bbm) AS bbm, SUM(biaya_supir) AS supir FROM transaksi_hutang WHERE tanggal_transaksi=?");$query->bindValue(1, $tggl);$query->execute();
		$tby = $query->fetchAll(); foreach($tby as $t){ $tobaya = $t['tobaya']; $supir = $t['supir']; $bbm = $t['bbm'];}
	}else{
	$bp = $qC->lihat_hutang23($bln);
	echo 'Tanggal Transaksi : '.date('F Y', strtotime($bln)).'</b><br><br>';
	$query = $pdo->prepare("SELECT SUM(total_biaya) AS tobaya, SUM(biaya_bbm) AS bbm, SUM(biaya_supir) AS supir FROM transaksi_hutang WHERE date_format(tanggal_transaksi,'%Y-%m')=?");$query->bindValue(1, $bln);$query->execute();
		$tby = $query->fetchAll(); foreach($tby as $t){ $tobaya = $t['tobaya']; $supir = $t['supir']; $bbm = $t['bbm'];}
	}
	echo '<table border=1 width="100%" class="tabel">
<tr class="trs"><td rowspan="2">Jenis Mobil</td><td rowspan="2">Penyewa</td><td rowspan="2">Tujuan</td><td rowspan="2">Tggl Kembali</td><td rowspan="2">Tarif</td><td rowspan="2">Uang Muka</td><td rowspan="2">Total Biaya</td><td rowspan="2">Sisa Hutang</td><td colspan="2">Biaya Tambahan</td><td rowspan="2">Pembayaran</td><td rowspan="2">Ket. Mobil</td></tr>
<tr class="trs"><td>Biaya BBM</td><td width="10%">Biaya Supir</td></tr>
';
foreach($bp as $data){
	$dp = $data['biaya_awal'];
	$toba = $data['total_biaya'];
	$total = $data['sisa_hutang'];
echo'<tr>
<td>'.$data['nama_mobil'].'</td>
<td>'.$data['nama'].'</td>
<td>'.$data['tujuan'].'</td>
<td>'.date('d F, Y', strtotime($data['tanggal_kembali'])).'</td>
<td>'.rp($data['tarif']).'</td>
<td>'.rp($dp).'</td>
<td>'.rp($toba).'</td>
<td>'.rp($total).'</td>
<td>'.rp($data['biaya_bbm']).'</td>
<td>'.rp($data['biaya_supir']).'</td>
<td>'.$data['status'].'</td>
<td>'.$data['status_mobil'].'</td>
</tr>';}?>
<tr class="tebel"><td colspan="6">Total</td><td><?php echo rp($tobaya); ?></td><td></td><td><?php echo rp($bbm); ?></td><td><?php echo rp($supir); ?></td><td colspan="2"></td></tr>
</table>
<!--CONTOH Code END-->
<?php }
$mpdf->AddPage('L','','','','',20,20,20,20,18,12);
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
