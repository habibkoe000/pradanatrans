<?php

 // Define relative path from this script to mPDF
 $nama_dokumen='help'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../asset/MPDF57/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8', 'A4'); 
$mpdf->SetDisplayMode('fullpage','two');// Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags
ob_start();
?>
<!--sekarang Tinggal Codeing seperti biasanya. HTML, CSS, PHP tidak masalah.-->
<!--CONTOH Code START-->
<link rel="stylesheet" href="../asset/MPDF57/mpdf.css" type="text/css">
<div class="jdl_lap">Buku Panduan Administrator</div>
<br>
1. Menu Branda : <br>
<div class="help_p">
- Menu branda menampilkan 7 laporan peminjaman terbaru, terdiri dari peminjaman tunai dan peminjaman hutang.<br>
- Untuk mencetak Infoice atau Kuitansi anda bisa mengklik Icon <img src="../gambar/print.png">.<br>
- Untuk mencetak 7 laporan terbaru , anda bisa mengklik tombol merah "Cetak".
</div>
<hr />
2. Menu Master Mobil : <br>
<div class="help_p">
- dalam menu ini terdapat submenu "Produsen Mobil", "Jenis Mobil", "Merk Mobil", "Warna Mobil", "Master Mobil".<br />
- "Produsen Mobil", Admin bisa menambah data produsen mobil dengan mengisi form kemudian klik tombol "simpan", Admin bisa menghapus data juga dengan klik icon <img src="../gambar/bs.png" /> atau mengedit dengan klik icon <img src="../gambar/edit.png" />.<br />
- "Jenis Mobil", Admin bisa menambah data Jenis mobil dengan mengisi form kemudian klik tombol "simpan", Admin bisa menghapus data juga dengan klik icon <img src="../gambar/bs.png" /> atau mengedit dengan klik icon <img src="../gambar/edit.png" />.<br />
- "Merk Mobil", Admin bisa menambah data merk mobil dengan memilih produsen mobil kemudian mengisi nama mobil pada form kemudian klik tombol "simpan", Admin bisa menghapus data juga dengan klik icon <img src="../gambar/bs.png" /> atau mengedit dengan klik icon <img src="../gambar/edit.png" />.<br />
- "Warna Mobil", Admin bisa menambah warna mobil dengan mengisi form kemudian klik tombol "simpan", Admin bisa menghapus data juga dengan klik icon <img src="../gambar/bs.png" /> atau mengedit dengan klik icon <img src="../gambar/edit.png" />.<br />
- "Master Mobil", Admin bisa menambah data mobil dengan mengisi semua form kemudian klik tombol "simpan".
</div>
<hr>
3. Menu Data Pelanggan : <br>
<div class="help_p">
- Menu Data Pelanggan menampilkan data dari pelanggan atau penyewa mobil.<br>
- Sebelum melakukan transaksi peminjaman, Admin terlebih dahulu masuk ke menu ini, bila data pelanggan belum ada, Admin harus menginputkan data penyewa terlebih dahulu.<br>
- Untuk melakukan penginputan data penyewa, Admin bisa mengklik tombol berwarna biru "Pelanggan Baru".<br>
- Bila data pelanggan sudah ada klik tombok merah "Pesan Mobil".
</div>
<hr>
4. Menu Data Mobil : <br>
<div class="help_p">
- Menu Data Mobil menampilakan semua data mobil yang "ready".<br>
- Untuk melihat detail mobil, cukup dengan mengklik gambar mobil.<br>
- Untuk mengubah data mobil atau menghapusnya, Admin bisa mengklik tombol warna biru "Edit".<br>
	<div class="help_p">
	- Bila sudah klik "Edit", terdapat icon <img src="../gambar/edit.png"> untuk merubah data mobil.<br>
    - selain bisa merubah, Admin juga bisa menghapus dengan mengklik icon <img src="../gambar/bs.png">.
	</div>
- Selain Tombol "Edit", di sampingnya juga terdapat tombol "Tambah", tombol ini digunakan untuk menambah data mobil.
</div>
<hr>
5. Menu Transaksi Peminjaman : <br>
<div class="help_p">
- Menu Transaksi Peminjaman ini berisi form untuk melakukan transaksi.<br>
- Setelah masuk ke menu ini, Admin bisa memilih penyewa yang akan melakukan penyewaan mobil, dengan syarat data penyewa sudah terinput terlebih dahulu.<br>
- Setelah itu memilih mobil yang di inginkan, setelah memilih mobil, Tarif dasar muncul secara otomatis.<br>
- Di samping kanan Admin bisa menentukan tanggal mobil di pinjam, dan tanggal mobil di kembalikan, setelah di set tanggalnya , maka secara otomatis total jam akan muncul secara otomatis.<br>
- pada bagian "Biaya Tambahan", Admin bisa mencentang bila :
	<div class="help_p">
	- Bahan bakar mobil sudah terisi oleh Pradana.<br>
    - Penyewa membutuhkan Supir.
	</div>
- untuk mengatur biaya BBM dan supir, Admin bisa mengklik tombol <img src="../gambar/edit.png">.  <br> 
- pada bagian "Biaya", Admin bisa memberikan Diskon secara manual.<br>
- untuk melihat Biaya Total, Admin cukup mengklik Textfild di samping tulisan "Biaya Total".<br>
- pada bagian "Pembayaran", bila penyewa melakukan transaksi tunai, maka jumlah uang yang dimasukkan harus sama dengan Biaya Total, tapi jika melakukan Transaksi Hutang, Admin bisa mamasukkan jumlah uang muka terlebih dahulu setelah itu klik tombol hitung.<br>
- Bila kolom "belum di bayar" terisi maka transaksi berupa transaksi hutang, namun bila kolom "belum di bayar" terisi 0, maka transaksi tunai.<br>
- pada bagian "Jenis Pembayaran", Admin wajib mencentang "cash" kalau transaksi tunai, dan "kredit" kalau transaksi hutang.
</div>
<hr>
<br><br>
6. Menu Data Mitra : <br>
<div class="help_p">
- Menu Data Mitra menampilkan data dari rental lain.<br>
- Pada menu ini sudah tersedia form untuk menambah data rental lain.<br>
- Di bawah form terdapat data dari rental lain.<br>
- Bila Admin mau mengubah data bisa mengklik icon <img src="../gambar/edit.png">.<br>
- Bila Admin mau menghapus data bisa mengklik icon <img src="../gambar/bs.png">.
</div>
<hr />
7. Menu Mobil Rental Lain : <br>
<div class="help_p">
- Menu Mobil Rental Lain menampilakan semua data mobil rental lain yang "ready".<br>
- Untuk melihat detail mobil, cukup dengan mengklik gambar mobil.<br>
- Untuk mengubah data mobil atau menghapusnya, Admin bisa mengklik tombol warna biru "Edit".<br>
	<div class="help_p">
	- Bila sudah klik "Edit", terdapat icon <img src="../gambar/edit.png"> untuk merubah data mobil.<br>
    - selain bisa merubah, Admin juga bisa menghapus dengan mengklik icon <img src="../gambar/bs.png">.
	</div>
- Selain Tombol "Edit", di sampingnya juga terdapat tombol "Tambah", tombol ini digunakan untuk menambah data mobil.
</div>
<hr />
8. Menu Transaksi Rental Lain : <br>
<div class="help_p">
- Menu Transaksi Peminjaman ini berisi form untuk melakukan transaksi.<br>
- Setelah masuk ke menu ini, Admin bisa memilih penyewa yang akan melakukan penyewaan mobil, dengan syarat data penyewa sudah terinput terlebih dahulu.<br>
- Setelah itu memilih Nama Rental yang di inginkan.<br>
- Setelah itu memilih mobil yang di inginkan, setelah memilih mobil, Tarif dasar muncul secara otomatis.<br>
- Di samping kanan Admin bisa menentukan tanggal mobil di pinjam, dan tanggal mobil di kembalikan, setelah di set tanggalnya , maka secara otomatis total jam akan muncul secara otomatis.<br>
- pada bagian "Biaya Tambahan", Admin bisa mencentang bila :
	<div class="help_p">
	- Bahan bakar mobil sudah terisi oleh Pradana.<br>
    - Penyewa membutuhkan Supir.
	</div>
- untuk mengatur biaya BBM dan supir, Admin bisa mengklik tombol <img src="../gambar/edit.png">.  <br> 
- pada bagian "Biaya", Admin bisa memberikan Diskon secara manual.<br>
- untuk melihat Biaya Total, Admin cukup mengklik Textfild di samping tulisan "Biaya Total".<br>
- pada bagian "Pembayaran", bila penyewa melakukan transaksi tunai, maka jumlah uang yang dimasukkan harus sama dengan Biaya Total, tapi jika melakukan Transaksi Hutang, Admin bisa mamasukkan jumlah uang muka terlebih dahulu setelah itu klik tombol hitung.<br>
- Bila kolom "belum di bayar" terisi maka transaksi berupa transaksi hutang, namun bila kolom "belum di bayar" terisi 0, maka transaksi tunai.<br>
- pada bagian "Jenis Pembayaran", Admin wajib mencentang "cash" kalau transaksi tunai, dan "kredit" kalau transaksi hutang.
</div>
<hr />
9. Menu Pengambalian Mobil : <br>
<div class="help_p">
- pada menu ini terdapat tombol berwarna biru "Mobil Sendiri", dan "Mobil Rental Lain".<br />
- "Mobil Sendiri", menampilkan data pengembalian mobil pradana yang di pinjam.<br />
- "Mobil Rental Lain", menampilkan data pengembalian mobil rental lain yang di pinjam.<br />
- selain itu terdapat tombol merah di sebelah kanan "Lunas", "Hutang".<br />
- tombol "lunas" menampilkan data mobil yang di sewa secara cash.<br />
- tombol "hutang" menampilkan data mobil yang di sewa secara hutang.<br />
- Untuk melakukan pengembalian mobil, cukup mengklik gambar mobil dan muncul form.<br />
- Setelah masuk dalam form, klik tombol "Cek Tanggal" untuk validasi tanggal kembali.<br />
- Setelah itu klik tombol "Mobil Kembali".
</div>
<hr />
10. Menu Lihat Peminjaman : <br>
<div class="help_p">
- dalam menu ini terdapat submenu "Tunai", "Hutang", "Rent Lain | Tunai", "Rent Lain | Hutang".<br />
- "Tunai", Admin bisa melihat data transaksi Penyewaan "mobil pradana" yang bersifat tunai / cash.<br />
- "Hutang, Admin bisa melihat data transaksi Penyewaan "mobil pradana" yang bersifat Hutang / kredit.<br />
- "Rent Lain | Tunai", Admin bisa melihat data transaksi Penyewaan "mobil rental lain" yang bersifat tunai / cash.<br />
- "Rent Lain | Hutang, Admin bisa melihat data transaksi Penyewaan "mobil rental lain" yang bersifat Hutang / kredit.<br />
- form cari berdasarkan bulan, untuk mencari data transaksi berdasarkan bulan.<br />
- form cari berdasarkan tanggal, untuk mencari dara transaksi berdasarkan tanggal.<br />
- untuk mencetak invoice / kuitansi bisa dengan mengklik icon <img src="../gambar/print.png" />.<br />
- untuk melakukan edit / merubah data transaksi bisa dengan mengklik icon <img src="../gambar/edit.png" />.<br />
- untuk menghapus data transaksi dengan mengklik icon <img src="../gambar/bs.png" />.<br />
- untuk mencetak semua data / data perbulan / data per tanggal bisa dengan mengklik tombol merah "Cetak Detail" di sudut kanan bawah. 
</div>
<hr />
11. Menu Akun Pengguna : <br>
<div class="help_p">
- dalam menu ini Admin bisa menambah Akun yang bisa mengakses halaman khusus seperti halaman kasir, admin dan marketing.<br />
- selain bisa menambah, Admin juga bisa mengedit dengan klik icon <img src="../gambar/edit.png" /> atau menghapus data dengan klik icon <img src="../gambar/bs.png" />.
</div>
<hr />
<br /><br /><br />
11. Menu Halaman Utama : <br>
<div class="help_p">
- dalam menu ini terdapat submenu "Slider", "Tambah Slider", "Kontak Kami", "Cara Pesan".<br />
- "Slider", berisi data Slider, disini Admin bisa menghapus data dengan klik icon <img src="../gambar/bs.png" /> atau mengedit dengan icon <img src="../gambar/edit.png" />.<br />
- "Tambah Slider", Admin bisa menambah data Slider pada menu ini.<br />
- "Kontak Kami", Admin bisa mengedit Info mengenai Pradana pada menu ini.<br />
- "Cara Pesan", Admin bisa mengedit tata cara pemesanan mobil pada menu ini.
</div>
<!--CONTOH Code END-->
<?php 
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
