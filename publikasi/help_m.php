<?php

 // Define relative path from this script to mPDF
 $nama_dokumen='help'; //Beri nama file PDF hasil.
define('_MPDF_PATH','../asset/MPDF57/');
include(_MPDF_PATH . "mpdf.php");
$mpdf=new mPDF('utf-8', 'A4'); 
$mpdf->SetDisplayMode('fullpage','two');// Create new mPDF Document
//Beginning Buffer to save PHP variables and HTML tags
ob_start();
?>
<!--sekarang Tinggal Codeing seperti biasanya. HTML, CSS, PHP tidak masalah.-->
<!--CONTOH Code START-->
<link rel="stylesheet" href="../asset/MPDF57/mpdf.css" type="text/css">
<div class="jdl_lap">Buku Panduan Marketing</div>
<br>
1. Menu Branda : <br>
<div class="help_p">
- Menu Data Mobil menampilakan semua data mobil yang "ready".<br>
- Untuk melihat detail mobil, cukup dengan mengklik gambar mobil.
</div>
<hr>
2. Menu Profil Perusahaan : <br>
<div class="help_p">
- Menu ini berisi profil dari perusahaan
</div>
<hr>
3. Menu Cara Pesan : <br>
<div class="help_p">
- Menu ini berisi info cara memesan mobil.
</div>
<hr />
4. Menu Akun Pengguna : <br>
<div class="help_p">
- dalam menu ini Marketing bisa merubah Akunnya sendiri.
</div>
<!--CONTOH Code END-->
<?php 
$html = ob_get_contents(); //Proses untuk mengambil hasil dari OB..
ob_end_clean();
//Here convert the encode for UTF-8, if you prefer the ISO-8859-1 just change for $mpdf->WriteHTML($html);
$mpdf->WriteHTML(utf8_encode($html));
$mpdf->Output($nama_dokumen.".pdf" ,'I');
exit;
?>
