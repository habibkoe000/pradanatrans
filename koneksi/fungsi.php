<?php 
function rp($angka){
	    $konversi = 'Rp '.number_format($angka, 0, ',', '.');
	    return $konversi;
	}
	
function hr($x){
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return hr($x - 10) . " belas";
  elseif ($x < 100)
    return hr($x / 10) . " puluh" . hr($x % 10);
  elseif ($x < 200)
    return " seratus" . hr($x - 100);
  elseif ($x < 1000)
    return hr($x / 100) . " ratus" . hr($x % 100);
  elseif ($x < 2000)
    return " seribu" . hr($x - 1000);
  elseif ($x < 1000000)
    return hr($x / 1000) . " ribu" . hr($x % 1000);
  elseif ($x < 1000000000)
    return hr($x / 1000000) . " juta" . hr($x % 1000000);
	
	}
?>
	