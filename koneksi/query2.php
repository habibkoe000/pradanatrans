<?php
class qC2{
	public function branda_peminjaman2(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan, tp.id_mst_mbl, mm.id_nm, tp.id_p, tp.tanggal_pinjam, tp.status_mobil, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif, tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.nomer_polisi) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC LIMIT 7");
		$query->execute();
		return $query->fetchAll();
	}
	public function mdl2($id_pd){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, mm.gambar,mm.id_m, mm.denda_d, tp.status_mobil,mm.nomer_polisi, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pd=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as mi ON t_n.id_m = mi.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->execute();
		return $query->fetch();
	}
	public function mobil_dipinjam_form2($id_p){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan, tp.id_mst_mbl, mm.id_nm, mm.denda_m, tp.id_p, tp.denda, mm.gambar_mobil, tp.status_mobil, mm.nomer_polisi, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.nomer_polisi WHERE tp.id_p=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function mobil_dipinjam2(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p,mm.gambar_mobil, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.nomer_polisi WHERE tp.status_mobil='dipinjam') as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC");
		$query->execute();
		return $query->fetchAll();
	}
}


?>