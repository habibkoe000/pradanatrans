<?php

class qC{
	public function warna_mobil(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM warna_mobil");$query->execute();
		return $query->fetchAll();
	}
	public function tujuan(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM tujuan ORDER BY tujuan ASC");$query->execute();
		return $query->fetchAll();
	}
	public function tjn_ed($id_t){
		global $pdo;$query = $pdo->prepare("SELECT * FROM tujuan WHERE id_t=?");$query->bindValue(1, $id_t);$query->execute();
		return $query->fetch();
	}
	public function b_t(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM b_t");$query->execute();
		return $query->fetch();
	}
	public function jenis_mobil(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM jenis_mobil");$query->execute();
		return $query->fetchAll();
	}
	public function total_tunai(){
		global $pdo;$query = $pdo->prepare("SELECT sum(total_biaya) FROM transaksi_peminjaman");$query->execute();
		return $query->fetch();
	}
	public function ttkm(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM tentang_kami");$query->execute();
		return $query->fetch();
	}
	public function crpsn(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM cara_pesan");$query->execute();
		return $query->fetch();
	}
	public function allslide(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM slider ORDER BY id_s DESC");$query->execute();
		return $query->fetchAll();
	}
	public function slidedpn(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM slider ORDER BY id_s DESC LIMIT 5");$query->execute();
		return $query->fetchAll();
	}
	public function slideside(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM slider ORDER BY id_s DESC LIMIT 3");$query->execute();
		return $query->fetchAll();
	}
	public function updateslide($id_s){
		global $pdo;$query = $pdo->prepare("SELECT * FROM slider WHERE id_s=?");
		$query->bindValue(1, $id_s);
		$query->execute();
		return $query->fetch();
	}
	public function detail_pelanggan($id_p){
		global $pdo;$query = $pdo->prepare("SELECT * FROM pelanggan WHERE id_pelanggan=?");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function wm_ed($id_w){
		global $pdo;$query = $pdo->prepare("SELECT * FROM warna_mobil WHERE id_warna=?");$query->bindValue(1, $id_w);$query->execute();return $query->fetch();
	}
	public function jn_ed($id_jn){
		global $pdo;$query = $pdo->prepare("SELECT * FROM jenis_mobil WHERE id_jn=?");$query->bindValue(1, $id_jn);$query->execute();return $query->fetch();
	}
	public function user(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM user");$query->execute();return $query->fetchAll();
	}
	public function mitra(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM mitra");$query->execute();return $query->fetchAll();
	}
	public function mitra_detail($id_m){
		global $pdo;$query = $pdo->prepare("SELECT * FROM mitra WHERE id_m=?");$query->bindValue(1, $id_m);$query->execute();return $query->fetch();
	}
	public function user_e($id_u){
		global $pdo;$query = $pdo->prepare("SELECT * FROM user WHERE id_u=?");$query->bindValue(1, $id_u);$query->execute();
		return $query->fetch();
	}
	public function pelanggan(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM pelanggan ORDER BY id_pelanggan DESC");$query->execute();return $query->fetchAll();
	}
	public function nama_mobil(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM nama_mobil LEFT JOIN produsen_mobil ON produsen_mobil.id_pm=nama_mobil.id_pm ORDER BY produsen_mobil.nama_produsen ASC");$query->execute();
		return $query->fetchAll();
	}
	public function nm_ed($id_nm){
		global $pdo;$query = $pdo->prepare("SELECT * FROM nama_mobil WHERE id_nm=?");$query->bindValue(1, $id_nm);$query->execute();return $query->fetch();
	}
	public function produsen_mobil(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM produsen_mobil");$query->execute();return $query->fetchAll();
	}
	public function pm_ed($id_pm){
		global $pdo;$query = $pdo->prepare("SELECT * FROM produsen_mobil WHERE id_pm=?");$query->bindValue(1, $id_pm);$query->execute();return $query->fetch();
	}
	public function master_mobil(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm WHERE status_mst_mbl='ready' and jns_pnjm='tujuan'");$query->execute();return $query->fetchAll();
	}
	public function master_mobil_id($id_jn){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm WHERE status_mst_mbl='ready' and jns_pnjm='tujuan' and master_mobil.id_jn=? ORDER BY master_mobil.tujuan ASC");$query->bindValue(1, $id_jn);$query->execute();return $query->fetchAll();
	}
	public function master_mobil_hr(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm WHERE status_mst_mbl='ready' and jns_pnjm='hari'");$query->execute();return $query->fetchAll();
	}
	public function mmb(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm WHERE master_mobil.tampil='Y' ORDER BY master_mobil.id_nm ASC LIMIT 4");$query->execute();return $query->fetchAll();
	}
	public function mmbdrop(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm GROUP BY master_mobil.id_nm");$query->execute();return $query->fetchAll();
	}
	public function smmb(){
		global $pdo;$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm WHERE status_mst_mbl='ready' ORDER BY master_mobil.gambar_mobil LIMIT 3");$query->execute();return $query->fetchAll();
	}
	public function mobil_dipinjam(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p,mm.gambar_mobil, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.status_mobil='dipinjam') as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function mobil_hutang_dipinjam(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h,mm.gambar_mobil, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.status_mobil='dipinjam') as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_h DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function mobil_hutang_dipinjam_f($id_h){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, mm.denda_m, tp.id_h,mm.gambar_mobil,mm.nomer_polisi,tp.biaya_awal, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_h=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_h);
		$query->execute();
		return $query->fetch();
	}
	public function mobil_dipinjam_form($id_p){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan, tp.id_mst_mbl, mm.id_nm, mm.denda_m, tp.id_p, tp.denda, mm.gambar_mobil, tp.status_mobil, mm.nomer_polisi, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_p=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function mobil_dipinjam_dmrl(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, mm.gambar,mm.id_m, tp.status_mobil, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.status_mobil='dipinjam') as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as mi ON t_n.id_m = mi.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_pd DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function mobil_hutang_dipinjam_dmrl(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_hd, mm.gambar,mm.id_m, tp.status_mobil, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.status_mobil='dipinjam') as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as mi ON t_n.id_m = mi.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_hd DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function mdl($id_pd){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, mm.gambar,mm.id_m, mm.denda_d, tp.status_mobil,mm.nomer_polisi, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pd=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as mi ON t_n.id_m = mi.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->execute();
		return $query->fetch();
	}
	public function mdlh($id_pd){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_hd, mm.gambar,mm.id_m,tp.biaya_awal, mm.denda_d, tp.status_mobil,mm.nomer_polisi, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_hd=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as mi ON t_n.id_m = mi.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->execute();
		return $query->fetch();
	}
	public function edit_mobil(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE jns_pnjm='tujuan'");
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_mobil_view(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE jns_pnjm='tujuan' GROUP BY master_mobil.nomer_polisi");
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_mobil_jn($id_jn){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna LEFT JOIN jenis_mobil ON jenis_mobil.id_jn=master_mobil.id_jn WHERE jns_pnjm='tujuan' and master_mobil.id_jn=?");
		$query->bindValue(1, $id_jn);
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_mobil_jn1($id_jn){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna LEFT JOIN jenis_mobil ON jenis_mobil.id_jn=master_mobil.id_jn WHERE jns_pnjm='tujuan' and master_mobil.id_jn=?");
		$query->bindValue(1, $id_jn);
		$query->execute();
		return $query->fetch();
	}
	public function edit_mobil_jn2($id_nm, $id_jn){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE jns_pnjm='tujuan' and master_mobil.id_nm=? and master_mobil.id_jn=?");
		$query->bindValue(1, $id_nm);
		$query->bindValue(2, $id_jn);
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_mobil_hr(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE jns_pnjm='hari'");
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_mobil_hr_view(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE jns_pnjm='hari' GROUP BY master_mobil.nomer_polisi");
		$query->execute();
		return $query->fetchAll();
	}
	public function edit_dmrl(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM dmrl LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=dmrl.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=dmrl.id_nm LEFT JOIN mitra ON mitra.id_m=dmrl.id_m");
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrl_mobil(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM dmrl LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=dmrl.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=dmrl.id_nm LEFT JOIN mitra ON mitra.id_m=dmrl.id_m WHERE status_dmrl='ready'");
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_peminjaman($tggl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, mm.nomer_polisi,tp.tanggal_pinjam,tp.status_mobil,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC");
		$query->bindValue(1, $tggl);
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_peminjaman23($bln){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, mm.nomer_polisi,tp.tanggal_pinjam,tp.status_mobil,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE date_format(tp.tanggal_transaksi,'%Y-%m')=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC");
		$query->bindValue(1, $bln);
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_peminjaman($tggl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm,mm.nomer_polisi, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, tp.tanggal_pinjam,tp.status_mobil,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_pd DESC");
		$query->bindValue(1, $tggl);
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_peminjaman23($bln){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm,mm.nomer_polisi, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, tp.tanggal_pinjam,tp.status_mobil,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE date_format(tp.tanggal_transaksi,'%Y-%m')=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_pd DESC");
		$query->bindValue(1, $bln);
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_peminjaman_semua(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.nomer_polisi, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_peminjaman_semua(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, mm.nomer_polisi,tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_pd, tp.status_mobil,tp.tanggal_pinjam,tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_pd DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_hutang($tggl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.sisa_hutang, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.biaya_awal, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_h DESC");
		$query->bindValue(1, $tggl);
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_hutang23($bln){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.sisa_hutang, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.biaya_awal, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE date_format(tp.tanggal_transaksi,'%Y-%m')=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_h DESC");
		$query->bindValue(1, $bln);
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_hutang($tggl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_hd, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.biaya_awal, tp.sisa_hutang, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_hd DESC");
		$query->bindValue(1, $tggl);
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_hutang23($bln){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_hd, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.biaya_awal, tp.sisa_hutang, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE date_format(tp.tanggal_transaksi,'%Y-%m')=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_hd DESC");
		$query->bindValue(1, $bln);
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrlinvoice_hutang($id_hd){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm,tp.id_m, tp.id_hd, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.biaya_awal, tp.diskon, tp.sisa_hutang, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_hd=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_hd);
		$query->execute();
		return $query->fetch();
	}
	public function dmrlinvoicea_hutang($id_hd,$tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm,tp.id_m, tp.id_hd, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.biaya_awal, tp.diskon, tp.sisa_hutang, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_hd);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetch();
	}
	public function dmrlinvoiceb_hutang($id_hd,$tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.status_mobil,tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm,tp.id_m, tp.id_hd, tp.tanggal_pinjam, tp.tanggal_transaksi, tp.biaya_awal, tp.diskon, tp.sisa_hutang, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_hd);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetchAll();
	}
	public function lihat_hutang_semua(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya,tp.biaya_awal, tp.sisa_hutang, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.status_mobil, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_h DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function dmrllihat_hutang_semua(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir,tp.sisa_hutang,tp.biaya_awal, tp.status, mm.status_dmrl, tp.id_pelanggan,tp.id_dmrl, mm.id_nm, tp.id_hd, tp.tanggal_pinjam,tp.tanggal_transaksi,tp.status_mobil, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_hd DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function branda_peminjaman(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p,mm.nomer_polisi, tp.tanggal_pinjam,tp.status_mobil, tp.tanggal_transaksi, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_p DESC LIMIT 7");
		$query->execute();
		return $query->fetchAll();
	}
	public function branda_hutang(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.status_mobil,tp.sisa_hutang,tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h,tp.biaya_awal, tp.tanggal_transaksi, tp.tanggal_pinjam, tp.tanggal_kembali, tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan
ORDER BY t_n.id_h DESC LIMIT 7");
		$query->execute();
		return $query->fetchAll();
	}
	public function invoice_peminjaman($id_p){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.diskon, tp.status, mm.nomer_polisi, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, tp.tanggal_pinjam, tp.nopol, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_p=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function invoiceb_peminjaman($id_p, $tgl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.diskon, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, tp.tanggal_pinjam, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->bindValue(2, $tgl);
		$query->execute();
		return $query->fetchAll();
	}
	public function invoicea_peminjaman($id_p, $tgl){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.diskon, tp.status, tp.nopol, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_p, tp.tanggal_pinjam, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_peminjaman as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->bindValue(2, $tgl);
		$query->execute();
		return $query->fetch();
	}
	public function dmrlinvoice_peminjaman($id_pd){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.diskon,tp.id_pelanggan,tp.id_dmrl,tp.id_m, mm.id_nm, tp.id_pd, tp.tanggal_pinjam, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pd=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->execute();
		return $query->fetch();
	}
	public function dmrlinvoicea_peminjaman($id_pd, $tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.diskon,tp.id_pelanggan,tp.id_dmrl,tp.id_m, mm.id_nm, tp.id_pd, tp.tanggal_pinjam, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pelanggan=? AND tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetch();
	}
	public function dmrlinvoiceb_peminjaman($id_pd, $tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.biaya_supir, tp.status, mm.status_dmrl, tp.diskon,tp.id_pelanggan,tp.id_dmrl,tp.id_m, mm.id_nm, tp.id_pd, tp.tanggal_pinjam, tp.tanggal_kembali,tp.tanggal_transaksi, tp.tarif,tp.tujuan FROM transaksi_dmrl as tp LEFT JOIN dmrl as mm ON tp.id_dmrl=mm.id_dmrl WHERE tp.id_pelanggan=? AND tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN mitra as m ON t_n.id_m = m.id_m
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_pd);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetchAll();
	}
	public function invoice_hutang($id_p){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.diskon, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.biaya_awal, tp.tanggal_pinjam,mm.nomer_polisi,tp.nopol, tp.tanggal_kembali, tp.sisa_hutang, tp.tanggal_transaksi,tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_h=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function invoicea_hutang($id_p,$tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.diskon, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.biaya_awal, tp.tanggal_pinjam, tp.tanggal_kembali, tp.sisa_hutang, tp.tanggal_transaksi,tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetch();
	}
	public function invoiceb_hutang($id_p, $tgll){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM (SELECT tp.total_biaya, tp.biaya_bbm, tp.diskon, tp.biaya_supir, tp.status, mm.status_mst_mbl, tp.id_pelanggan,tp.id_mst_mbl, mm.id_nm, tp.id_h, tp.biaya_awal, tp.tanggal_pinjam, tp.tanggal_kembali, tp.sisa_hutang, tp.tanggal_transaksi,tp.tarif,tp.tujuan FROM transaksi_hutang as tp LEFT JOIN master_mobil as mm ON tp.id_mst_mbl=mm.id_mst_mbl WHERE tp.id_pelanggan=? and tp.tanggal_transaksi=?) as t_n 
LEFT JOIN nama_mobil as s ON t_n.id_nm = s.id_nm
LEFT JOIN pelanggan as d ON t_n.id_pelanggan = d.id_pelanggan");
		$query->bindValue(1, $id_p);
		$query->bindValue(2, $tgll);
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_master_mobil($id){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE id_mst_mbl=?");
		$query->bindValue(1, $id);
		$query->execute();
		return $query->fetch();
	}
	public function detail_master_mobil_hr($id){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM master_mobil LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=master_mobil.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=master_mobil.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=master_mobil.id_warna WHERE id_mst_mbl=?");
		$query->bindValue(1, $id);
		$query->execute();
		return $query->fetch();
	}
	public function detail_dmrl_mobil($id){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM dmrl LEFT JOIN produsen_mobil ON 
		produsen_mobil.id_pm=dmrl.id_pm LEFT JOIN nama_mobil ON nama_mobil.id_nm=dmrl.id_nm LEFT JOIN warna_mobil ON warna_mobil.id_warna=dmrl.warna WHERE id_dmrl=?");
		$query->bindValue(1, $id);
		$query->execute();
		return $query->fetch();
	}

}
?>