<?php 
include_once('koneksi/koneksi.php');
include_once('koneksi/query.php');
$qC = new qC;
$mmb = $qC->mmb();$smmb = $qC->smmb();$slide = $qC->slidedpn();$master_mobil = $qC->master_mobil();$slideside = $qC->slideside();
$mmbdrop = $qC->mmbdrop();
$pg = $_GET['page']; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Pradana Trans</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="pradana transport rental mobil murah dan nyaman">
<meta name="keywords" content="rental mobil, pradana transport, pradana trans">
<meta name="author" content="habib koe tkj">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/supersized.core.css" type="text/css" media="screen">  
<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/camera.css" type="text/css" media="screen">


<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/supersized.core.3.2.1.js"></script>

<script type="text/javascript" src="js/camera.js"></script>
<script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
<script>
$(document).ready(function() {
 	// camera
	$('#camera_wrap').camera({
		//thumbnails: true
		autoAdvance			: true,		
		mobileAutoAdvance	: true,
		height: '43%',
		hover: false,
		loader: 'none',
		navigation: false,
		navigationHover: false,
		mobileNavHover: false,
		playPause: false,
		pauseOnClick: false,
		pagination			: true,
		time: 7000,
		transPeriod: 1000,
		minHeight: '150px'
	});

}); //
$(window).load(function() {
	//

}); //
</script>		
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>

<body class="main">
<div id="main">
<div id="inner">

<div class="container">
<div class="row">
<div class="span12">
<header class="clearfix">
	<div class="logo_wrapper"><a href="?page=branda" class="logo">Pradana Trans</a><br>
    Rent Car & Microbus
    </div>
	
</header>
<!-- do not delete or change--><div class="small_link"><a href="http://www.myfreecsstemplates.com" class="copy_link"></a></div><!-- end -->	
</div>	
</div>	
</div>

<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
				<li><a href="?page=branda">Beranda</a></li>				
				<li><a href="?page=tentang_kami">Info Kami</a></li>	
				<li><a href="?page=cara_pesan">Cara Pesan</a></li>							
				<li class="sub-menu sub-menu-1"><a href="">Mobil<em></em></a>
					<ul>						
						<?php foreach($mmbdrop as $mm){ ?> 
                      <li><a href="?page=detail_mobil&id=<?php echo $mm['id_mst_mbl']; ?>"><?php echo $mm['nama_mobil']; ?></a></li>
                      <?php } ?>																
					</ul>						
				</li>										
			</ul>
		</div>
	</div>
</div>	
</div>	
</div>	
</div>
<?php if($pg == 'branda'){include('page/hal_depan.php');
		}else if($pg=='detail_mobil'){include 'page/detail_mobil.php';
		}else if($pg=='pesan'){include 'page/pesan_mobil.php';
		}else if($pg=='tentang_kami'){include 'page/tentang_kami.php';
		}else if($pg=='cara_pesan'){include 'page/cara.php';
		}else if($pg=='semua_mobil'){include 'page/semua_mobil.php';
		}else if($pg=='detail_slide'){include 'page/detail_slide.php';
		} ?>

<footer>
<div class="container">
<div class="row">
<div class="span12">
<div class="bot2">
	<div class="pad_bottom">
		<div class="copyright">Copyright © <?php echo $d = date('Y'); ?>. <a href="http://pradanatrans.com">Pradana Trans.</a></div>
	</div>
</div>	
</div>	
</div>	
</div>
</footer>
</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>