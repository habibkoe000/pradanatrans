 <form>
    <fieldset class="jcalendar">
       <div class="jcalendar-wrapper">
       <div class="jcalendar-selects">
         <select name="day" id="day" class="jcalendar-select-day">
         <option value="0"></option>
         <?php for ($i=1;$i<=31;$i++){ 
		 echo ' <option value="'.$i.'">'.$i.'</option>'; }
		 ?>
         </select>
         <select name="month" id="month" class="jcalendar-select-month">
           <option value="0"></option>
           <option value="1">January</option>
           <option value="2">February</option>
           <option value="3">March</option>
           <option value="4">April</option>
           <option value="5">May</option>
           <option value="6">June</option>
           <option value="7">July</option>
           <option value="8">August</option>
           <option value="9">September</option>
           <option value="10">October</option>
           <option value="11">November</option>
           <option value="12">December</option>
         </select>
         <select name="year" id="year" class="jcalendar-select-year">
           <option value="0"></option>
            <?php for ($i=1980;$i<=2035;$i++){ 
		 echo ' <option value="'.$i.'">'.$i.'</option>'; }
		 ?>
         </select>
       </div>
       </div>
    </fieldset>
    </form>