-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2015 at 03:04 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rental_mobil`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_t`
--

CREATE TABLE IF NOT EXISTS `b_t` (
  `id_bt` int(11) NOT NULL AUTO_INCREMENT,
  `bbm` varchar(100) NOT NULL,
  `supir` varchar(100) NOT NULL,
  PRIMARY KEY (`id_bt`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_t`
--

INSERT INTO `b_t` (`id_bt`, `bbm`, `supir`) VALUES
(1, '90000', '150000');

-- --------------------------------------------------------

--
-- Table structure for table `cara_pesan`
--

CREATE TABLE IF NOT EXISTS `cara_pesan` (
  `id_cp` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_cp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cara_pesan`
--

INSERT INTO `cara_pesan` (`id_cp`, `judul`, `deskripsi`) VALUES
(1, 'Cara Pemesanan Mobil', '<p>Untuk melakukan pemesanan mobil bisa melakukan transaksi langsung melalui cara di bawah ini</p>\r\n<p>1. Telp/Fax :&nbsp;<strong>&nbsp;031 8782313</strong></p>\r\n<p>2. langsung ke Alamat kami :<strong>&nbsp;Jalan Medokan Sawah 63&nbsp;</strong></p>\r\n<p><strong>3. langsung ke kantor</strong></p>\r\n<p><strong>4. ke jargir</strong></p>');

-- --------------------------------------------------------

--
-- Table structure for table `dmrl`
--

CREATE TABLE IF NOT EXISTS `dmrl` (
  `id_dmrl` int(11) NOT NULL AUTO_INCREMENT,
  `id_m` int(11) NOT NULL,
  `id_pm` int(11) NOT NULL,
  `id_nm` int(11) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `nomer_polisi` varchar(100) NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `status_dmrl` varchar(100) NOT NULL,
  `denda_d` varchar(100) NOT NULL,
  `id_jn` int(11) NOT NULL,
  PRIMARY KEY (`id_dmrl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `dmrl`
--

INSERT INTO `dmrl` (`id_dmrl`, `id_m`, `id_pm`, `id_nm`, `warna`, `nomer_polisi`, `tarif`, `keterangan`, `gambar`, `status_dmrl`, `denda_d`, `id_jn`) VALUES
(1, 1, 13, 17, '6', 'L 2222 LU', '240000', 'menampung 8 orang', '1415426564-apv_frnt.png', 'ready', '5000', 2),
(3, 5, 14, 23, '2', 'L 2224 LU', '240000', 'Bisa menampung 9 orang', '1415505286-Mobil-Toyota-Innova.jpg', 'ready', '5000', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_mobil`
--

CREATE TABLE IF NOT EXISTS `jenis_mobil` (
  `id_jn` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jn`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jenis_mobil`
--

INSERT INTO `jenis_mobil` (`id_jn`, `jenis`) VALUES
(1, 'Premium Car'),
(2, 'SUV'),
(4, 'BUS');

-- --------------------------------------------------------

--
-- Table structure for table `master_mobil`
--

CREATE TABLE IF NOT EXISTS `master_mobil` (
  `id_mst_mbl` int(11) NOT NULL AUTO_INCREMENT,
  `id_pm` int(11) NOT NULL,
  `id_nm` int(11) NOT NULL,
  `tahun_keluar` varchar(10) NOT NULL,
  `bahan_bakar` varchar(50) NOT NULL,
  `nomer_polisi` varchar(50) NOT NULL,
  `id_warna` int(11) NOT NULL,
  `nama_pemilik` varchar(100) NOT NULL,
  `gambar_mobil` varchar(200) NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status_mst_mbl` varchar(30) NOT NULL,
  `id_jn` int(11) NOT NULL,
  `denda_m` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `min_hari` varchar(10) NOT NULL,
  `jns_pnjm` varchar(50) NOT NULL,
  `tggl_pajak` date NOT NULL,
  `ganti_olimesin` date NOT NULL,
  `ganti_oligarda` date NOT NULL,
  `km` varchar(100) NOT NULL,
  `tampil` varchar(20) NOT NULL,
  PRIMARY KEY (`id_mst_mbl`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `master_mobil`
--

INSERT INTO `master_mobil` (`id_mst_mbl`, `id_pm`, `id_nm`, `tahun_keluar`, `bahan_bakar`, `nomer_polisi`, `id_warna`, `nama_pemilik`, `gambar_mobil`, `tarif`, `keterangan`, `status_mst_mbl`, `id_jn`, `denda_m`, `tujuan`, `min_hari`, `jns_pnjm`, `tggl_pajak`, `ganti_olimesin`, `ganti_oligarda`, `km`, `tampil`) VALUES
(14, 14, 20, '2013', 'Premium', 'L 1111 LI', 5, 'Muhammad Habiburrahman', '1415426174-alpard.JPG', '270000', 'Mampu menampung 10 orang', 'ready', 1, '6000', 'Lamongan', '1', 'tujuan', '2014-12-17', '2014-12-17', '2014-12-17', '100', ''),
(16, 14, 19, '2013', 'Premium', 'L 1113 LI', 5, 'Muhammad Habiburrahman', '1415426403-harga-avanza-bekas.jpg', '800000', 'menampung 7 orang', 'ready', 2, '400000', 'Banyuwangi', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '200', 'Y'),
(17, 7, 8, '2000', '', 'L 1114 LI', 3, '', '1416749038-harga-avanza-bekas.jpg', '250000', '', 'ready', 2, '250000', 'Jember', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '150', ''),
(18, 14, 20, '2014', 'Premium', 'L 1111 LI', 4, 'Muhammad Habiburrahman', '1418170066-alpard.JPG', '800000', 'Nampung banyak', 'ready', 1, '400000', 'Kediri', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '200', 'Y'),
(19, 13, 21, '2013', 'Premium', 'L CR2 LM', 2, 'Muhammad Habiburrahman', '1418430656-sx4.jpg', '400000', 'bisa menampung 5 orang, mobil nyaman dipakai kemanapun, dengan berbagai kelebihan yang bisa memanjakan penumpang', 'ready', 1, '200000', '-', '', 'hari', '2014-12-18', '2014-12-18', '2014-12-18', '100', ''),
(20, 15, 25, '2013', 'Premium', 'L HY21 KL', 5, 'Muhammad Habiburrahman', '1418506504-hyundai sonata.jpg', '400000', 'Mobil ini memiliki daya tahan yang sangat tangguh.', 'ready', 5, '400000', '-', '', 'hari', '0000-00-00', '0000-00-00', '0000-00-00', '300', 'Y'),
(22, 18, 26, '2013', 'Premium', 'L KI12 FG', 4, 'Muhammad Habiburrahman', '1418507598-kia.jpg', '600000', 'Mobil ini mampu memberikan kenyamanan dan kepuasan yang sangat baik untuk anda, mampu menampung penumpang sebanyak 5 orang, dan memiliki sistem keamanan yang sangat baik', 'ready', 5, '300000', 'Lamongan', '1', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '350', 'Y'),
(23, 14, 28, '1994', 'Premium', 'L CM12 LM', 2, 'Muhammad Habiburrahman', '1418658350-sx4.jpg', '1000000', 'htehe', 'ready', 1, '500000', 'Demak', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '250', ''),
(24, 14, 20, '2013', 'Premium', 'L 1111 LI', 5, 'Muhammad Habiburrahman', '1418896164-alpard.JPG', '1000000', 'bisa ngapung', 'ready', 1, '500000', 'Demak', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '170', ''),
(25, 7, 15, '2010', 'solar', 'L JB21 LJ', 5, 'Muhammad Habiburrahman', 'mobil.jpg', '1350000', 'nampung banyak', 'ready', 4, '500000', '-', '', 'hari', '0000-00-00', '0000-00-00', '0000-00-00', '340', ''),
(26, 14, 27, '2013', 'Premium', 'L VF12 LK', 3, 'Muhammad Habiburrahman', 'mobil.jpg', '700000', 'tes ya', 'ready', 1, '300000', 'Madura', '1', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '230', ''),
(28, 14, 27, '2013', 'Premium', 'L VF12 LK', 3, 'Muhammad Habiburrahman', 'mobil.jpg', '1000000', 'kren abis', 'ready', 1, '300000', 'Kediri', '2', 'tujuan', '0000-00-00', '0000-00-00', '0000-00-00', '340', ''),
(29, 14, 23, '2011', 'Premium', 'L IN00 KG', 5, 'Muhammad Habiburrahman', 'mobil.jpg', '300000', 'keren', 'ready', 2, '150000', 'Gresik', '1', 'tujuan', '2014-12-17', '2014-12-17', '2014-12-17', '160', ''),
(30, 11, 16, '2012', 'Premium', 'L MOB0 HL', 3, 'Muhammad Habiburrahman', 'mobil.jpg', '300000', 'bagus', 'ready', 2, '150000', '-', '', 'hari', '2014-12-18', '2014-12-18', '2014-12-18', '400', ''),
(31, 14, 20, '2013', 'Premium', 'L 1111 LI', 5, 'Muhammad Habiburrahman', 'mobil.jpg', '1500000', 'keren', 'ready', 1, '750000', 'Bali', '3', 'tujuan', '2014-12-26', '2014-12-26', '2014-12-26', '500', ''),
(32, 14, 28, '2012', 'Premium', 'L CM12 LM', 3, 'Muhammad Habiburrahman', 'mobil.jpg', '2000000', 'keren', 'ready', 1, '1000000', 'Bandung', '4', 'tujuan', '2014-12-26', '2014-12-26', '2014-12-26', '800', ''),
(33, 2, 29, '2013', 'Premium', 'L XE23 KN', 2, 'Muhammad Habiburrahman', '1420788590-xenia.jpg', '1500000', 'bagus', 'ready', 2, '750000', 'Lombok', '5', 'tujuan', '2015-01-08', '2015-01-08', '2015-01-08', '450', ''),
(34, 9, 13, '2013', 'Pertmax', 'L E400 LK', 3, 'Muhammad Habiburrahman', 'mobil.jpg', '1500000', 'Keren', 'ready', 1, '500000', '-', '', 'hari', '2015-01-16', '2015-01-16', '2015-01-16', '', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `mitra`
--

CREATE TABLE IF NOT EXISTS `mitra` (
  `id_m` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mitra` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_telpon` varchar(100) NOT NULL,
  PRIMARY KEY (`id_m`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mitra`
--

INSERT INTO `mitra` (`id_m`, `nama_mitra`, `alamat`, `no_telpon`) VALUES
(1, 'Prima Trans', 'Rungkut', '0317890982'),
(2, 'Trajaya Indo', 'Darmo', '031789800'),
(5, 'Darma Trans', 'Rungkut ', '087978689009');

-- --------------------------------------------------------

--
-- Table structure for table `nama_mobil`
--

CREATE TABLE IF NOT EXISTS `nama_mobil` (
  `id_pm` int(11) NOT NULL,
  `id_nm` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mobil` varchar(100) NOT NULL,
  PRIMARY KEY (`id_nm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `nama_mobil`
--

INSERT INTO `nama_mobil` (`id_pm`, `id_nm`, `nama_mobil`) VALUES
(7, 8, 'panter'),
(9, 13, 'E400'),
(7, 15, 'Jetbus2'),
(11, 16, 'mobilio'),
(13, 17, 'AVP Arena'),
(14, 19, 'Avanza'),
(14, 20, 'Alphard'),
(13, 21, 'SX4'),
(14, 22, 'Crown Royal'),
(14, 23, 'Innova'),
(15, 25, 'Sonata'),
(18, 26, 'Optima'),
(14, 27, 'Vellfire'),
(14, 28, 'All New Camry'),
(2, 29, 'Xenia'),
(7, 30, 'Elf');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE IF NOT EXISTS `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `no_telpon` varchar(50) NOT NULL,
  `no_identitas` varchar(100) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama`, `alamat`, `no_telpon`, `no_identitas`) VALUES
(3, 'Sulthan Alvian', 'Terawangan', '08298398292', '100028041994'),
(4, 'Mursidin ', 'Anjani', '023104091829', '100010199283'),
(5, 'Mawar Mirzani', 'Jalan Jagir Sidomukti 2 no 2', '089287897898', '10001201001000'),
(6, 'Rina Anggraini', 'Rungkut', '0890981989189', '1000300909019'),
(7, 'Novita Astuti', 'Darmo', '03178909', '10004000909209'),
(8, 'Sri Hidyawati', 'Perian', '02178908', '1000400898918'),
(9, 'Arjuna Kirana', 'Jalan Sidosermo Indah', '087398479829739', '1000309209092'),
(10, 'Syukron Makmun', 'Medokan BArat', '021897868998', '10004009898298'),
(11, 'Syamsul Hadi Mukmin', 'Desa Paok Motong', '0876787827877', '90087979191719'),
(12, 'Latifha Handayani', 'Reban Tebu', '089729728789', '100989829892898'),
(13, 'Fitriana', 'Sandubaaya selong', '098768877898', '10099879800988'),
(14, 'Yusriani', 'perian', '0876890887877', '007780999090888'),
(15, 'Yunaili Hidayatun', 'Perian', '0897826878367', '1000998297978281');

-- --------------------------------------------------------

--
-- Table structure for table `produsen_mobil`
--

CREATE TABLE IF NOT EXISTS `produsen_mobil` (
  `id_pm` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produsen` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `produsen_mobil`
--

INSERT INTO `produsen_mobil` (`id_pm`, `nama_produsen`) VALUES
(2, 'daihatsu'),
(5, 'mazda'),
(7, 'isuzu'),
(9, 'mercedez'),
(11, 'honda'),
(13, 'suzuki'),
(14, 'toyota'),
(15, 'Hyundai'),
(16, 'Mitsubishi'),
(17, 'Nissan'),
(18, 'Kia');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id_s` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(150) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  PRIMARY KEY (`id_s`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_s`, `judul`, `deskripsi`, `gambar`) VALUES
(2, 'Sunrise Bromo', '<p>Di gunung bromo hal yang sangat di tunggu-tunggu adalah melihat sang surya terbit dari timmur yang begitu cantik sekali,,, sungguh cantik melihat nya secara langsung di antara pegunungan yang cantik juga,</p>', '1414307089-slide.jpg'),
(3, 'Jembatan Tol Suramadu', '<p>Jembatan ini merupakan Tol yang menghubungkan surabaya dengan madura, untuk sampai ke madura melewati tol ini cuman butuh waktu sekitar 15 menit.</p>', '1414306791-smdslide.jpg'),
(4, 'Selecta', '<p>Selecta berada di kota malang , tempat ini merupakan taman bunga yang memiliki banyak sekali koleksi, anda akan senang bermanja-manja untuk berpoto bersama bungaa-bunga yang indah nan cantik ini,&nbsp;</p>\r\n<p>dengan warna yang banyak, seakan di taman luar negeri, membuat anda betah berada di sini, untuk menikmati bunga-bunga yang cantik.</p>', '1414263191-selecta.jpg'),
(5, 'Bromo', '<p>Bromo di probolinggo</p>', '1414263244-bromo.jpg'),
(6, 'Ranukumbolo', '<p>berada di kaki gunung Smeru</p>', '1414263343-IMG_0200.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `tentang_kami`
--

CREATE TABLE IF NOT EXISTS `tentang_kami` (
  `id_t` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tentang_kami`
--

INSERT INTO `tentang_kami` (`id_t`, `judul`, `deskripsi`) VALUES
(1, 'Info Kami', '<p>Kami adalah perusahan penyedia peyewaan mobil sekala nasional,&nbsp;</p>\r\n<p>kami memiliki banyak armada yang bisa membantu para pelanggan untuk memenuhi kebutuhan dalam hal transportasi.</p>\r\n<p>kami beralamat pada<strong>&nbsp;Jalan Medokan Sawah 63</strong> dan bisa menghubungi kami melalui <strong>Telp/Fax : 031 8782313</strong></p>\r\n<p>&nbsp;</p>\r\n<p>rungkut indah&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_dmrl`
--

CREATE TABLE IF NOT EXISTS `transaksi_dmrl` (
  `id_pd` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `id_dmrl` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tujuan` varchar(200) NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `total_biaya` varchar(150) NOT NULL,
  `diskon` varchar(100) NOT NULL,
  `biaya_bbm` varchar(150) NOT NULL,
  `biaya_supir` varchar(150) NOT NULL,
  `kelebihan_waktu` varchar(100) NOT NULL,
  `denda` varchar(100) NOT NULL,
  `id_m` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `status_mobil` varchar(100) NOT NULL,
  `tot_hari` varchar(50) NOT NULL,
  `nopol` varchar(50) NOT NULL,
  `kasir` int(11) NOT NULL,
  PRIMARY KEY (`id_pd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `transaksi_dmrl`
--

INSERT INTO `transaksi_dmrl` (`id_pd`, `id_pelanggan`, `id_dmrl`, `tanggal_transaksi`, `tanggal_pinjam`, `tujuan`, `tanggal_kembali`, `tarif`, `total_biaya`, `diskon`, `biaya_bbm`, `biaya_supir`, `kelebihan_waktu`, `denda`, `id_m`, `status`, `status_mobil`, `tot_hari`, `nopol`, `kasir`) VALUES
(1, 9, 1, '2014-11-08', '2014-11-08 19:00:00', 'Malang 2', '2014-11-09 19:00:00', '10000', '315000', '0', '0', '75000', '0', '0', 1, 'lunas', 'kembali', '', '', 4),
(3, 6, 1, '2014-11-10', '2014-11-09 12:00:00', 'Malang 3', '2014-11-10 12:00:00', '10000', '315000', '0', '0', '75000', '11', '55000', 1, 'lunas', 'kembali', '', '', 4),
(4, 10, 3, '2014-11-09', '2014-11-09 16:00:00', 'Blitar', '2014-11-10 16:00:00', '10000', '240000', '0', '0', '0', '11', '55000', 5, 'lunas', 'kembali', '', '', 3),
(5, 9, 1, '2014-11-22', '2014-11-22 19:00:00', 'Sidoarjo', '2014-11-23 19:59:00', '240000', '400000', '0', '160000', '0', '2', '10000', 1, 'lunas', 'kembali', '', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_hutang`
--

CREATE TABLE IF NOT EXISTS `transaksi_hutang` (
  `id_h` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `id_mst_mbl` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tujuan` varchar(150) NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `biaya_awal` varchar(100) NOT NULL,
  `total_biaya` varchar(100) NOT NULL,
  `diskon` varchar(100) NOT NULL,
  `biaya_bbm` varchar(150) NOT NULL,
  `biaya_supir` varchar(150) NOT NULL,
  `sisa_hutang` varchar(100) NOT NULL,
  `kelebihan_waktu` varchar(100) NOT NULL,
  `denda` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `status_mobil` varchar(100) NOT NULL,
  `tot_hari` varchar(50) NOT NULL,
  `nopol` varchar(50) NOT NULL,
  `kasir` int(11) NOT NULL,
  PRIMARY KEY (`id_h`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `transaksi_hutang`
--

INSERT INTO `transaksi_hutang` (`id_h`, `id_pelanggan`, `id_mst_mbl`, `tanggal_transaksi`, `tanggal_pinjam`, `tujuan`, `tanggal_kembali`, `tarif`, `biaya_awal`, `total_biaya`, `diskon`, `biaya_bbm`, `biaya_supir`, `sisa_hutang`, `kelebihan_waktu`, `denda`, `keterangan`, `status`, `status_mobil`, `tot_hari`, `nopol`, `kasir`) VALUES
(1, 8, 16, '2014-11-08', '2014-11-08 19:00:00', 'Blitar', '2014-11-09 19:00:00', '12000', '288000', '288000', '0', '0', '0', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(2, 4, 14, '2014-11-25', '2014-11-26 07:00:00', 'Lamongan', '2014-11-27 07:00:00', '270000', '200000', '420000', '0', '0', '150000', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 3),
(4, 12, 16, '2014-12-09', '2014-12-10 07:00:00', 'Jakarta', '2014-12-11 07:00:00', '240000', '300000', '330000', '0', '90000', '0', '30000', '', '', 'pradana trans', 'hutang', 'kembali', '', '', 4),
(5, 6, 22, '2014-12-13', '2014-12-14 07:00:00', 'Lamongan', '2014-12-15 07:00:00', '600000', '500000', '600000', '0', 'include', 'include', '0', '3', '900000', 'pradana trans', 'lunas', 'kembali', '1', 'L KI12 FG', 4),
(6, 5, 14, '2014-12-13', '2014-12-15 07:00:00', 'Lamongan', '2014-12-16 07:00:00', '270000', '150000', '270000', '0', 'include', 'include', '0', '2', '12000', 'pradana trans', 'lunas', 'kembali', '1', 'L 1111 LI', 4),
(7, 13, 19, '2014-12-13', '2014-12-14 08:00:00', 'Blitar', '2014-12-15 08:00:00', '400000', '400000', '650000', '0', '100000', '150000', '0', '3', '600000', 'pradana trans', 'lunas', 'kembali', '1', 'L CR2 LM', 4);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_hutang_dmrl`
--

CREATE TABLE IF NOT EXISTS `transaksi_hutang_dmrl` (
  `id_hd` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `id_dmrl` int(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tujuan` varchar(150) NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `biaya_awal` varchar(100) NOT NULL,
  `total_biaya` varchar(100) NOT NULL,
  `diskon` varchar(100) NOT NULL,
  `biaya_bbm` varchar(100) NOT NULL,
  `biaya_supir` varchar(100) NOT NULL,
  `sisa_hutang` varchar(100) NOT NULL,
  `kelebihan_waktu` varchar(100) NOT NULL,
  `denda` varchar(100) NOT NULL,
  `id_m` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `status_mobil` varchar(100) NOT NULL,
  `tot_hari` varchar(50) NOT NULL,
  `nopol` varchar(50) NOT NULL,
  `kasir` int(11) NOT NULL,
  PRIMARY KEY (`id_hd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_peminjaman`
--

CREATE TABLE IF NOT EXISTS `transaksi_peminjaman` (
  `id_p` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `id_mst_mbl` varchar(11) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `tarif` varchar(100) NOT NULL,
  `total_biaya` varchar(100) NOT NULL,
  `diskon` varchar(100) NOT NULL,
  `biaya_bbm` varchar(100) NOT NULL,
  `biaya_supir` varchar(100) NOT NULL,
  `kelebihan_waktu` varchar(50) NOT NULL,
  `denda` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `status_mobil` varchar(100) NOT NULL,
  `tot_hari` varchar(50) NOT NULL,
  `nopol` varchar(50) NOT NULL,
  `kasir` int(11) NOT NULL,
  PRIMARY KEY (`id_p`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `transaksi_peminjaman`
--

INSERT INTO `transaksi_peminjaman` (`id_p`, `id_pelanggan`, `id_mst_mbl`, `tanggal_transaksi`, `tanggal_pinjam`, `tujuan`, `tanggal_kembali`, `tarif`, `total_biaya`, `diskon`, `biaya_bbm`, `biaya_supir`, `kelebihan_waktu`, `denda`, `keterangan`, `status`, `status_mobil`, `tot_hari`, `nopol`, `kasir`) VALUES
(4, 14, '14', '2014-11-22', '2014-11-21 15:00:00', 'Malang', '2014-11-23 15:00:00', '270000', '630000', '0', '160000', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(5, 12, '17', '2014-11-25', '2014-11-25 19:00:00', 'Jember', '2014-11-29 19:59:00', '250000', '1240000', '0', '240000', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(6, 7, '17', '2014-11-25', '2014-11-25 19:00:00', 'Jember', '2014-11-27 19:00:00', '250000', '740000', '0', '240000', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(7, 10, '16', '2014-11-25', '2014-11-25 17:00:00', 'Banyuwangi', '2014-11-27 17:59:00', '240000', '630000', '0', '0', '150000', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(8, 4, '17', '2014-12-08', '2014-11-26 07:00:00', 'Jember', '2014-11-29 07:00:00', '250000', '900000', '0', '0', '150000', '11', '2750000', 'pradana trans', 'lunas', 'kembali', '', '', 3),
(9, 11, '14', '2014-12-09', '2014-12-12 19:00:00', 'Lamongan', '2014-12-14 19:00:00', '270000', '630000', '0', '240000', '0', '0', '0', 'pradana trans', 'lunas', 'kembali', '', '', 4),
(17, 14, '18', '2014-12-12', '2014-12-12 09:00:00', 'Kediri', '2014-12-14 09:00:00', '800000', '850000', '0', '50000', 'include', '0', '0', 'pradana trans', 'lunas', 'kembali', '2', 'L 1111 LI', 4),
(18, 10, '20', '2014-12-13', '2014-12-15 07:00:00', 'Malang', '2014-12-17 07:00:00', '400000', '950000', '0', '50000', '100000', '1', '400000', 'pradana trans', 'lunas', 'kembali', '2', 'L HY21 KL', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tujuan`
--

CREATE TABLE IF NOT EXISTS `tujuan` (
  `id_t` int(11) NOT NULL AUTO_INCREMENT,
  `tujuan` varchar(100) NOT NULL,
  `min_hari` varchar(100) NOT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tujuan`
--

INSERT INTO `tujuan` (`id_t`, `tujuan`, `min_hari`) VALUES
(1, 'Malang', '1'),
(2, 'Surabaya', '1'),
(4, 'Blitar', '1'),
(5, 'Mojokerto', '1'),
(6, 'Pasuruan', '1'),
(7, 'Probolinggo', '2'),
(8, 'Jember', '2'),
(9, 'Banyuwangi', '2'),
(10, 'Jogjakarta', '2'),
(11, 'Bandung', '4'),
(12, 'Semarang', '2'),
(13, 'Gresik', '1'),
(14, 'Lamongan', '1'),
(15, 'Jakarta', '3'),
(16, 'Kediri', '2'),
(17, 'Ponorogo', '2'),
(18, 'Bali', '3'),
(19, 'Purwakarta', '3'),
(20, 'Solo', '2'),
(21, 'Temanggung', '2'),
(22, 'Lombok', '5'),
(23, 'Pacet', '1'),
(24, 'Pacitan', '4'),
(25, 'Demak', '2'),
(26, 'Kudus', '2'),
(27, 'Magelang', '3'),
(28, 'Tasikmalaya', '4'),
(29, 'Cilegon', '3'),
(30, 'Ciputat', '3'),
(32, 'Madura', '1'),
(33, 'Palembang', '7');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_u` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `hak_akses` varchar(100) NOT NULL,
  PRIMARY KEY (`id_u`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_u`, `nama`, `user`, `password`, `hak_akses`) VALUES
(1, 'Muhammad Habiburrahman', 'habib', '1391921ec73a2f5dff54e075bbda7487', 'admin'),
(2, 'Ulfia Nisrin', 'ririn', 'b84a4059d1af6c8b50bb7a28290dbd63', 'marketing'),
(3, 'Sulthan Alvian', 'vian', '2ceefdecaeaba09fd7d342302d91debf', 'kasir'),
(4, 'administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(6, 'Latifha Handayani', 'fha', '09564c3e4dd01f7b2492b6ff120de662', 'kasir');

-- --------------------------------------------------------

--
-- Table structure for table `warna_mobil`
--

CREATE TABLE IF NOT EXISTS `warna_mobil` (
  `id_warna` int(11) NOT NULL AUTO_INCREMENT,
  `warna` varchar(50) NOT NULL,
  PRIMARY KEY (`id_warna`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `warna_mobil`
--

INSERT INTO `warna_mobil` (`id_warna`, `warna`) VALUES
(2, 'silver'),
(3, 'hitam'),
(4, 'biru'),
(5, 'putih'),
(6, 'Coklat'),
(7, 'merah');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
