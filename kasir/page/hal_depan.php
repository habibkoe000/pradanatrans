<h1>Data  Peminjaman Terbaru Tunai</h1>
<table class="tabel_mbl" border="1" bordercolor="#CCCCCC">
<tr class="tr2"><td>Transaksi</td><td>Jenis Mobil</td><td>Penyewa</td><td>Total Biaya</td><td>Pembayaran</td><td>Ket. mobil</td><td>Invoice</td></tr>
<?php
function rp($angka){
	    $konversi = 'Rp '.number_format($angka, 2, ',', '.');
	    return $konversi;
	}
$no;
foreach($bp as $data){
	echo'<tr>
<td>'.date('d F, Y', strtotime($data['tanggal_transaksi'])).'</td>
<td>'.$data['nama_mobil'].'</td>
<td>'.$data['nama'].'</td>
<td>'.rp($data['total_biaya']).'</td>
<td>'.$data['status'].'</td>
<td>'.$data['status_mobil'].'</td>
<td><a href="../publikasi/invoice.php?id_p='.$data['id_pelanggan'].'&tgl='.$data['tanggal_transaksi'].'" target="_blank"><div class="icn_prn"></div></a></td>
</tr>';
}?>
</table>
<a href="../publikasi/file_pdf.php" target="_blank"><div class="next">Cetak</div></a>
<br /><br /><br /><hr />
<h1>Data  Peminjaman Terbaru Kredit</h1>
<table class="tabel_mbl" border="1" bordercolor="#CCCCCC">
<tr class="tr2"><td>Tanggal</td><td>Jenis Mobil</td><td>Penyewa</td><td>Uang Muka</td><td>Total Biaya</td><td>Pembayaran</td><td>Ket. mobil</td><td>Invoice</td></tr>
<?php
$no;
foreach($b_hut as $data){
	$dp = $data['biaya_awal'];
	$toba = $data['total_biaya'];
	$total = $toba - $dp;
	echo'<tr>
<td>'.date('d F, Y', strtotime($data['tanggal_transaksi'])).'</td>
<td>'.$data['nama_mobil'].'</td>
<td>'.$data['nama'].'</td>
<td>'.rp($dp).'</td>
<td>'.rp($data['total_biaya']).'</td>
<td>'.$data['status'].'</td>
<td>'.$data['status_mobil'].'</td>
<td><a href="../publikasi/invoice_h.php?id_p='.$data['id_pelanggan'].'&tgl='.$data['tanggal_transaksi'].'" target="_blank"><div class="icn_prn"></div></a></td>
</tr>';
}
?>
</table>
<a href="../publikasi/file_hutang.php" target="_blank"><div class="next">Cetak</div></a>